import styled from 'styled-components/native';

export const Title = styled.Text`
  color: ${props => props.theme.font};
  text-align: center;
  font-family: ${ (props) => props.fontFamily };
  font-size: ${ (props) => props.fontSize }px;
  top: ${ (props) => props.top }px;
`;
