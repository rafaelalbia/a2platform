import styled from 'styled-components/native';

export const ContainerColumn = styled.View`
  color: #fff;
  font-size: ${ (props) => props.fontSize };
  height: ${ (props) => props.height };
  width: ${ (props) => props.width };
  margin-top: ${ (props) => props.marginTop };
  margin-right: ${ (props) => props.marginRight };
  margin-bottom: ${ (props) => props.marginBottom };
  margin-left: ${ (props) => props.marginLeft };
  align-items: ${ (props) => props.alignItems };
  flex-direction: column;
  justify-content: ${ (props) => props.justifyContent };
  top: ${ (props) => props.top };
`;
