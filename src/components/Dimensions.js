import { Dimensions } from 'react-native';

export var height = Dimensions.get('window').height;
export var width = Dimensions.get('window').width;
