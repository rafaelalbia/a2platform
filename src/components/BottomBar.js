import React from 'react';
import styled from 'styled-components/native';
import { height, width } from './Dimensions';

const BottomBarBase = styled.View`
  background-color: rgba(255, 255, 255, 0.4);
  height: ${height * 0.0062}px;
  width: ${width * 0.3333}px;
  border-radius: 4px;
  bottom: ${height * 0.0025}%;
  position: absolute;
`;

export default function BottomBar() {
  return (
    <BottomBarBase />
  );
}
