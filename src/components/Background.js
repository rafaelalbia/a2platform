import styled from 'styled-components/native';

export const Background = styled.ImageBackground`
  height: 100%;
  width: 100%;
  align-items: center;
`;
