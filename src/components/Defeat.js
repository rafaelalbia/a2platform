import React from 'react';
import styled from 'styled-components/native';
import { height, width } from './Dimensions';

export const DefeatBase = styled.View`
  width: 100%;
  align-items: center;
`;

DefeatBase.Image = styled.Image`
  top: ${ (props) => props.top };
`;
