import styled from 'styled-components/native';

export const ExperienceBar = styled.View`
  background-color: ${props => props.theme.primary};
  height: 15px;
  width: 100%;
  border-radius: 10px;
  margin-top: 4px;
`;

ExperienceBar.Content = styled.View`
  background-color: ${props => props.theme.secondary};
  height: 15px;
  width: ${ (props) => props.width };
  border-radius: 10px;
`;