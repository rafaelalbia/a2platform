import styled from 'styled-components/native';
import { width } from './Dimensions';

export const ModuleBenefits = styled.View`
  width: ${width * 0.7638}px;
  flex-direction: row;
  justify-content: space-between;
  margin-top: ${ (props) => props.marginTop };
  align-items: center;
`;

ModuleBenefits.Icon = styled.Image`
`;

ModuleBenefits.Description = styled.Text`
  color: ${props => props.theme.font};
  font-family: 'Raleway-Regular';
  width: ${ (props) => props.width };
`;
