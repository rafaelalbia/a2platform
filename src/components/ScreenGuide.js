import styled from 'styled-components/native';
import { height } from './Dimensions';

export const Guide = styled.View`
  border-radius: 90px;
  height: ${height * 0.0187}px;
  width: ${height * 0.0187}px;
  background: rgba(11, 6, 23, 0.5);;
`;

Guide.Selected = styled.View`
  background: #fff;
`;