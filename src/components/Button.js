import { Colors } from 'react-native/Libraries/NewAppScreen';
import styled from 'styled-components/native'
import { height, width } from './Dimensions'

export const Button = styled.View`
  background: ${props => props.theme.primary};
  border-radius: 27px;
  height: ${height * 0.0718}px;
  width: ${width * 0.8638}px;
  align-items: center;
  justify-content: center;
  top: ${ (props) => props.top };
  padding-bottom: ${ (props => props.paddingBottom) };
`;

Button.Text = styled.Text`
  color: ${props => props.theme.font};
  font-family: ${ (props) => props.fontFamily };
  font-size: ${ (props) => props.fontSize };
`;
