import styled from 'styled-components/native';

export const ContainerRow = styled.View`
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  top: ${ (props) => props.top };
  width: ${ (props) => props.width };
`;
