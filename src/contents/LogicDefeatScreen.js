import React from 'react';
import { Image, Text } from 'react-native';
import { Background } from '../components/Background';
import BottomBar from '../components/BottomBar';
import { Button } from '../components/Button';
import { ContainerRow } from '../components/ContainerRow';
import { ContainerColumn } from '../components/ContainerColumn';
import { height, width } from '../components/Dimensions';
import { ExperienceBar } from '../components/ExperienceBar';
import { DefeatBase } from '../components/Defeat';
import { ModuleBenefits } from '../components/ModuleBenefits';
import { Guide } from '../components/ScreenGuide';
import { Title } from '../components/Title';

export const LogicDefeatScreen = function() {
  return (
    <>
      <Background source={require('../images/background-logic.png')}>


        <DefeatBase>

          <ContainerRow
            top={`${20}px`}
            width={'95%'}
          >

            <DefeatBase.Image
              source={require('../images/arrow.png')}
              top={0}
            />

            <DefeatBase.Image
              source={require('../images/config.png')}
              top={0}
            />

          </ContainerRow>

          <Title
            fontSize={width * 0.1}
            fontFamily={'Raleway-Bold'}
            top={height * 0.1046}
          >
            Sorry,{"\n"} try again!
          </Title>

          <DefeatBase.Image 
            source={require('../images/sad.png')} 
            top={`${height * 0.1875}px`}
          />

        </DefeatBase>

        <Button
            top={`${height * 0.35}px`}
            paddingBottom={`${height * 0.004}px`}
          >
          <Button.Text
            fontFamily={'Raleway-Bold'}
            fontSize={`${width * 0.05}px`}
          >
            Quero tentar denovo!
          </Button.Text>
        </Button>

        <ContainerRow
          top={`${height * 0.4555}px`}
          width={`${(width * 0.0194) + (height * 0.0187) * 2}px`}
        >
          <Guide />
          <Guide />
        </ContainerRow>

        <BottomBar />

      </Background>
    </>
  );
}
