import React from 'react';
import { Image, Text } from 'react-native';
import { Background } from '../components/Background';
import BottomBar from '../components/BottomBar';
import { ContainerRow } from '../components/ContainerRow';
import { ContainerColumn } from '../components/ContainerColumn';
import { height, width } from '../components/Dimensions';
import { Title } from '../components/Title';

export const InitialScreen = function() {
  return (
    <>
      <Background source={require('../images/background-start.png')}>

        <Title
          fontSize={width * 0.0666}
          fontFamily={'Raleway-Bold'}
          top={height * 0.335}
        >
        Quem você prefere ser?
        </Title>

        <ContainerRow
          top={`${height * 0.39}px`}
          width={'auto'}
        >

          <ContainerColumn
            fontSize={`${width * 0.0333}px`}
            height={`${height * 0.1968}px`}
            width={`${width * 0.2472}px`}
            marginTop={`${0}px`}
            marginLeft={`${8}px`}
            marginBottom={`${0}px`}
            marginRight={`${8}px`}
            alignItems={'center'}
            justifyContent={'space-between'}
            top={`${0}px`}
          >
            <Image source={require(`../images/icon-commercial-logic.png`)} />
            <Text style={{
              color: '#fff',
              textAlign: 'center',
              fontFamily: 'Raleway-SemiBold'
            }}>
              Sherlock Holmes
            </Text>
          </ContainerColumn>

          <ContainerColumn
            fontSize={`${width * 0.0333}px`}
            height={`${height * 0.1968}px`}
            width={`${width * 0.2472}px`}
            marginTop={`${0}px`}
            marginLeft={`${8}px`}
            marginBottom={`${0}px`}
            marginRight={`${8}px`}
            alignItems={'center'}
            justifyContent={'space-between'}
            top={`${0}px`}
          >
            <Image source={require(`../images/icon-commercial-creativity.png`)} />
            <Text style={{
              color: '#fff',
              textAlign: 'center',
              fontFamily: 'Raleway-SemiBold'
            }}>
              Leonardo Da Vinci
            </Text>
          </ContainerColumn>

          <ContainerColumn
            fontSize={`${width * 0.0333}px`}
            height={`${height * 0.1968}px`}
            width={`${width * 0.2472}px`}
            marginTop={`${0}px`}
            marginLeft={`${8}px`}
            marginBottom={`${0}px`}
            marginRight={`${8}px`}
            alignItems={'center'}
            justifyContent={'space-between'}
            top={`${0}px`}
          >
            <Image source={require(`../images/icon-commercial-communication.png`)} />
            <Text style={{
              color: '#fff',
              textAlign: 'center',
              fontFamily: 'Raleway-SemiBold'
            }}>
              Mahatma Gahdi
            </Text>
          </ContainerColumn>

        </ContainerRow>

        <BottomBar
          height={height * 0.0062}
          width={width * 0.3333}
          bottom={height * 0.0025}
        />

      </Background>
    </>
  );
}
