import React from 'react';
import { Image, Text } from 'react-native';
import { Background } from '../components/Background';
import BottomBar from '../components/BottomBar';
import { Button } from '../components/Button';
import { ContainerRow } from '../components/ContainerRow';
import { ContainerColumn } from '../components/ContainerColumn';
import { height, width } from '../components/Dimensions';
import { ExperienceBar } from '../components/ExperienceBar';
import { ModuleBenefits } from '../components/ModuleBenefits';
import { Title } from '../components/Title';

export const CreativityScreen = function() {
  return (
    <>
      <Background source={require('../images/background-creativity.png')}>

        <Title
          fontSize={width * 0.0666}
          fontFamily={'Raleway-Bold'}
          top={height * 0.0875}
        >
          Comunicação
        </Title>

        <ContainerColumn
          fontSize={`${width * 0.0388}px`}
          height={`${191}px`}
          width={`${width * 0.6777}px`}
          marginTop={`${0}px`}
          marginLeft={`${0}px`}
          marginBottom={`${0}px`}
          marginRight={`${0}px`}
          alignItems={'center'}
          justifyContent={'space-between'}
          top={`${height * 0.1093}px`}
        >
          <Image source={require(`../images/icon-creativity.png`)} />
          <Text style={{
            color: `#ffffff`,
            textAlign: 'center',
            fontFamily: 'Raleway-Regular'
          }}>
            Teste suas habilidades de criatividade com exercícios simples e práticos. É hora de melhorar sua criatividade!
          </Text>
        </ContainerColumn>

        <ContainerColumn
          fontSize={`${width * 0.0333}px`}
          height={`${55}px`}
          width={`${width * 0.8833}px`}
          marginTop={`${0}px`}
          marginLeft={`${0}px`}
          marginBottom={`${0}px`}
          marginRight={`${0}px`}
          alignItems={'null'}
          justifyContent={'null'}
          top={`${height * 0.1328}px`}
        >
          <Text style={{
            color: '#fff',
            fontFamily: 'Raleway-Regular'
          }}>SEU PROGRESSO</Text>
          <ExperienceBar>
            <ExperienceBar.Content width={`${50}%`} />
          </ExperienceBar>
          <ContainerRow
            top={`${0}px`}
            width={'auto'}
          >
            <Text style={{
              color: '#fff',
              fontFamily: 'Raleway-Regular'
            }}>
              0
            </Text>
            <Text style={{
              color: '#fff',
              fontFamily: 'Raleway-Regular'
            }}>
              1
            </Text>
          </ContainerRow>
        </ContainerColumn>

        <ContainerColumn
          fontSize={`${width * 0.0333}px`}
          height={`${height * 0.1984}px`}
          width={`${width * 0.8833}px`}
          marginTop={`${0}px`}
          marginLeft={`${0}px`}
          marginBottom={`${0}px`}
          marginRight={`${0}px`}
          alignItems={'null'}
          justifyContent={'null'}
          top={`${height * 0.1875}px`}
        >
          <Text style={{
            color: '#fff',
            fontFamily: 'Raleway-Regular'
          }}>
            BENEFÍCIOS:
          </Text>
        </ContainerColumn>

        <ContainerColumn
          fontSize={`${width * 0.0333}px`}
          height={`${height * 0.1984}px`}
          width={`${width * 0.8833}px`}
          marginTop={`${0}px`}
          marginLeft={`${0}px`}
          marginBottom={`${0}px`}
          marginRight={`${0}px`}
          alignItems={'null'}
          justifyContent={'null'}
          top={'null'}
        >
          <ModuleBenefits
            marginTop={`${height * 0.0387}px`}
          >
            <ModuleBenefits.Icon
              source={require('../images/creativity.png')}
            />
            <ModuleBenefits.Description
              width={`${width * 0.5972}px`}
            >
              Aprimore suas habilidades de criatividade com exercícios simples
            </ModuleBenefits.Description>
          </ModuleBenefits>

          <ModuleBenefits
            marginTop={`${height * 0.0187}px`}
          >
            <ModuleBenefits.Icon
              source={require('../images/light-bulb.png')}
            />
            <ModuleBenefits.Description
              width={`${width * 0.5972}px`}
            >
              Aprenda como pensar fora da caixa e encontre novas soluções para o mesmo problema
            </ModuleBenefits.Description>
          </ModuleBenefits>
        </ContainerColumn>

        <Button
          top={`${height * 0.07}px`}
          paddingBottom={`${height * 0.004}px`}
        >
          <Button.Text
            fontFamily={'Raleway-Bold'}
            fontSize={`${width * 0.05}px`}
          >
            Estou pronto!
          </Button.Text>
        </Button>

        <BottomBar />

      </Background>
    </>
  );
}
