import React, { Component } from 'react';
import { InitialScreen } from '../components/InitialScreenContent'

export default class App extends Component {
  render () {
    return (
      <InitialScreen />
    );
  }
}
