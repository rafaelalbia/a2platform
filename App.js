import React from 'react';
import { ThemeProvider } from 'styled-components';
import { InitialScreen } from './src/contents/InitialScreenContent';
import { LogicScreen } from './src/contents/LogicScreenContent';
import { CommunicationScreen } from './src/contents/CommunicationScreenContent';
import { CreativityScreen } from './src/contents/CreativityScreenContent';
import { MemoryScreen } from './src/contents/MemoryScreenContent';
import { LogicDefeatScreen } from './src/contents/LogicDefeatScreen';

const colors = {
  font: "#FFFFFF",
  primary: "#3D3C64",
  secondary: "#40FFFF"
};

export default function App() {
  return (
    <ThemeProvider theme={colors}>
      <LogicDefeatScreen />
    </ThemeProvider>
  );
}
